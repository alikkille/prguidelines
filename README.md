# Pull request guidelines #

![Overview](prguidelines/prguidelines/media/img/overview.gif)

Pull request guidelines is just a checklist for your repos. It helps you
and your team follow a common set of guidelines for development and for
reviewing work. It's a mechanism to avoid making the same mistakes
repeatedly and to follow the best practices in your team.

To learn more, visit the
[Pull request guidelines homepage](https://prguidelines.atlassian.io/).

## Installing Pull request guidelines ##

Installing the add-on on your Bitbucket account is easy. Just open [this](
https://bitbucket.org/site/addons/authorize?descriptor_uri=https%3A//prguidelines.atlassian.io/bitbucket/descriptor.json&redirect_uri=https%3A//prguidelines.atlassian.io/installation_redirect
) in a new tab to go to the installation dialog, choose the account you want
to install the add-on to and hit "Grant access".

## Running it locally ##

To develop prguidelines, you will need to run it locally and install that
locally served add-on to Bitbucket. This section goes over that process
step-by-step.

Make sure you have docker setup locally and are able to build docker images.
For more details on this, visit
[the Docker docs](https://docs.docker.com/mac/).

To build the docker image, run:

```bash
make dev
```

This should build your Dockerfile (found in `build/Dockerfile`) and the
corresponding docker image. After this step is completed, you should be able
to see your image by running:

```bash
docker images | grep prguidelines
```

Once the images are built, you should be able to launch the server locally
using:

```bash
docker-compose up
```

You should then be able to visit the homepage of the add-on by running:

```bash
open http://$(docker-machine ip default):8080
```

Alternatively, you could manually go to `http://<ip-address>:8080` in your
browser, where `<ip-address>` is the IP address returned by running
`docker-machine ip default` in your browser.

### Setting up ngrok ###

For the locally running add-on to work on Bitbucket.org, Bitbucket needs to
be able to contact your server. The easiest way to accomplish that is to use
[ngrok](https://ngrok.com/). Set it up locally using the instructions on
Ngrok's [Download and Installation guide](https://ngrok.com/#download).

Once you're able to run ngrok start a HTTP tunnel using

```bash
ngrok http $(docker-machine ip default):8080
```

The resulting screen should show you the externally accessible URL of
the HTTP tunnel. Confirm that it works by visiting the URL in your browser.

### Creating the OAuth consumer ###

To install your locally running add-on, you need to create an OAuth consumer
on Bitbucket. See the [Bitbucket OAuth docs](
https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html)
to figure out how to do that. The account with which you create the OAuth
consumer will be considered the creator of the add-on. Make sure the
callback URL in the consumer form is set to the HTTPS version of the
externally accessible ngrok URL. Also, make sure to select the
`pullrequest:write` scope since the add-on will need to be able to create
comments and tasks on the pull requests.

Now make a copy of `prguidelines/prguidelines/settings/dev_dist.py` in the
same directory as `dev_dist.py` and call it `dev.py`. Update the
`BB_OAUTH_CONSUMER_KEY` and `BB_OAUTH_CONSUMER_SECRET` to be the actual
consumer key and secret from your Bitbucket account.


### Installing the add-on in Bitbucket ###

Go to the settings of the account that you wish to install this add-on on.
Remember that this account could be either a user or a team. Then navigate to
"Manage add-ons" and click on "Install add-on from URL". In the dialog that
pops up enter the HTTPS version of the ngrok URL for your add-on and hit
install. Follow the prompts and your add-on should now be installed. The
pull request guidelines webitem link should be present on all PRs.


## Contributing ##

Just make a pull request and follow the `CONTRIBUTING.md` guidelines (coming
soon).