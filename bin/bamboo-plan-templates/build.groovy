plan(key:'GBUILD', name:'PRGuidelines build', description:'Build the docker container for PR Guidelines and run tests') {
    def repositoryName = 'prguidelines'

    project(key:'GUIDE', name:'prguidelines')

    repository(name: repositoryName)

    trigger(type:'polling',strategy:'periodically',frequency:'180') {
        repository(name: repositoryName)
    }

    permissions() {
        loggedInUser(permissions:'read,build')
    }

    if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
        // Not on depbac - monitor branch builds
        branchMonitoring(notificationStrategy: 'INHERIT') {
            createBranch(matchingPattern: '.*')
            inactiveBranchCleanup(periodInDays: '10')
            deletedBranchCleanup(periodInDays: '1')
        }
    }

    stage(name:'Build docker image', description:'Create the docker image') {
        job(key:'DOCK', name:'Build docker image', description:'Build docker image') {
            requirement(key:'os', condition:'equals', value:'Linux')

            task(type:'checkout', description:'Check out Default Repository') {
                repository(name: repositoryName)
            }

            task(type:'script', description:'Run makefile', scriptBody:'''
                make
            '''.stripIndent())
        }
    }
}
