# Basic config
SERVICE_NAME=prguidelines
BUILD_DIR=build
THIS_COMMIT=$(shell git rev-parse HEAD)

# Micros related stuff
DEPLOYMENT_ENV=ddev
SD_TMPL=$(SERVICE_NAME).sd.yml.tmpl
SD=$(BUILD_DIR)/$(SERVICE_NAME).sd.yml

# Docker related stuff
DOCKER_SERVER=docker.atlassian.io
DOCKER_REMOTE_PATH=$(DOCKER_SERVER)/atlassian
VERSION=$(THIS_COMMIT)
DOCKER_REMOTE=$(DOCKER_REMOTE_PATH)/$(SERVICE_NAME):$(VERSION)
DOCKERFILE=Dockerfile
DOCKERFILE_TMPL=$(DOCKERFILE).tmpl
DOCKERFILE_PTH=$(BUILD_DIR)/$(DOCKERFILE)
REQUIREMENTS=$(shell grep -v '^\#' requirements.txt)

all: build

dev: dockercompose-build

build: docker-build sd-build

dockerfile-build:
	mkdir -p $(BUILD_DIR)
	m4 -DREQUIREMENTS="$(REQUIREMENTS)" $(DOCKERFILE_TMPL) > $(DOCKERFILE_PTH)

sd-build:
	m4 -DTAG=$(VERSION) $(SD_TMPL) > $(SD)

docker-build: dockerfile-build
	docker build -t $(SERVICE_NAME) -f $(DOCKERFILE_PTH) .
	docker tag $(SERVICE_NAME) $(DOCKER_REMOTE)

dockercompose-build: dockerfile-build
	docker-compose build

deploy:
	docker push $(DOCKER_REMOTE)
	micros service:deploy -e $(DEPLOYMENT_ENV) -f $(SD) $(SERVICE_NAME)

clean:
	rm -rf build

.PHONY: build deploy clean
