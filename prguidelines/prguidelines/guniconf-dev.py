import multiprocessing

bind = '0.0.0.0:8080'
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'sync'
accesslog = "-"
errorlog = "-"
logfile = "-"
loglevel = "info"
logger_class = 'prguidelines.apps.micros.logs.GunicornLogger'
timeout = 0
reload = True
sendfile = False


def post_worker_init(worker):
    from django.core.urlresolvers import resolve

    try:
        resolve('/micros/healthcheck')
    except:
        # It doesn't matter that it succeeds. This is just to warm-up.
        pass
