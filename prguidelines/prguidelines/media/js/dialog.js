$(function () {
  // Register the close button click event
  $('header > .aui-dialog2-header-close').click(function () {
    if ($('body').data('refreshNeeded')) {
      AP.require('history', function (history) {
        history.go(0);
      });
    }
    AP.require('dialog', function (dialog) {
      dialog.close();
    });
  });

  // Register tooltips for all relevant buttons
  AJS.$('#more-info-icon').tooltip({gravity: 'w'});
});
