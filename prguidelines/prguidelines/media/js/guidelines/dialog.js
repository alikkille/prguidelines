$(function () {
  var addonUrl = location.protocol + '//' + location.hostname;

  // Register tooltips for all relevant buttons
  AJS.$('.quote-button').tooltip({gravity: 'w'});

  // Allow guidelines to be expanded
  $('.section-title-text').click(function() {
    var $section = $(this);
    var $content = $section.parent().siblings('.guideline-section-content');

    // If the section that got clicked is already visible, then ignore
    if($content.is(":hidden")) {
      // First, slide up all the section contents
      $('.guideline-section-content').slideUp();

      // Then, slide down the relevant content section
      $content.slideDown();
    }
  });

  // Register clicks for quoting
  $('.guideline-section .quote-button').click(function () {
    var $button = $(this);
    var icon = $button.find('.aui-icon');
    var $section = $button.closest('.guideline-section');
    var title = $section.find('h4').text().trim();
    var $contentDiv = $section.find('.guideline-section-content');
    var content = $contentDiv.data('section-content-raw');
    var sourceUrl = $contentDiv.data('source-url');
    var pathParts = location.pathname.split('/');
    var repoUuid = pathParts[3];
    var prId = pathParts[4];

    var $successIcon = $button.siblings('.success-icon');
    var $spinnerContainer = $button.siblings('.spinner-container');

    // Replace the button with the spinner
    $button.hide();
    $spinnerContainer.spin();

    AP.require('request', function (request) {
      request({
        url: '1.0/repositories/{}/' + repoUuid + '/pullrequests/' + prId + '/comments',
        type: 'POST',
        data: JSON.stringify({
          content: 'Please make sure to follow this guideline:\n\n# ' +
          title + '\n' + content + '\n' +
          '###### Referenced from [CONTRIBUTING.md](' + sourceUrl + ') ' +
          'by [Pull request guidelines add-on](' + addonUrl + ').'
        }),
        contentType: 'application/json',
        success: function (response) {
          $('body').data('refreshNeeded', true);
          var commentId = response.comment_id;
          request({
            url: 'internal/repositories/{}/' + repoUuid + '/pullrequests/' + prId + '/tasks',
            type: 'POST',
            data: JSON.stringify({
              comment: {id: commentId},
              content: {
                raw: "Adhere to guideline: " + title
              }
            }),
            contentType: 'application/json',
            error: function (error) {
              console.log(error);
              $button.show();
              $spinnerContainer.spinStop();
            },
            success: function () {
              $spinnerContainer.spinStop();
              $spinnerContainer.hide();
              $successIcon.show();
            }
          });
        },
        error: function (error) {
          console.log(error);
          $button.show();
          $spinnerContainer.spinStop();
        }
      });
    });
  });
});
