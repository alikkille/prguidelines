from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'prguidelines',
        'USER': 'prguidelines',
    }
}

LOGGING['root']['level'] = 'DEBUG'

# The OAuth consumer key and secret for local development
BB_OAUTH_CONSUMER_KEY = ''
BB_OAUTH_CONSUMER_SECRET = ''

# The domain name the addon is hosted on
CANON_URL = 'http://192.168.99.100:8080'
BITBUCKET_URL = 'https://bitbucket.org'
