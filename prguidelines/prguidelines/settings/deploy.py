import dj_database_url

from .base import *


DEBUG = TEMPLATE_DEBUG = False

SECRET_KEY = os.environ['SECRET_KEY']

DATABASES = {
    'default': dj_database_url.parse(os.environ['PG_PRGUIDELINES_URL'])
}


LOGGING['root'] = {
    'level': 'INFO',
}

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

# TODO: Remove this if it really works.
BB_OAUTH_CONSUMER_KEY = os.environ['BB_OAUTH_CONSUMER_KEY']
BB_OAUTH_CONSUMER_SECRET = os.environ['BB_OAUTH_CONSUMER_SECRET']

CANON_URL = os.environ['CANON_URL']
BITBUCKET_URL = os.environ['BITBUCKET_URL']

# This app is probably not very susceptible to host-header exploits. Plus, this
# setting is a pain to maintain with ELB healthchecks and micros's own stuff
# and Django not allowing IP ranges to be specified here.
ALLOWED_HOSTS = ['*']
