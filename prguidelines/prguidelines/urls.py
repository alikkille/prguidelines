from django.conf.urls import include, url


urlpatterns = [
    url(r'^', include('prguidelines.apps.help.urls')),
    url(r'^bitbucket/', include('prguidelines.apps.bitbucket.urls')),
    url(r'^guidelines/', include('prguidelines.apps.guidelines.urls')),
    url(r'^micros/', include('prguidelines.apps.micros.urls')),
]
