from django.conf.urls import url
from prguidelines.apps.bitbucket import views


urlpatterns = [
    url(r'^descriptor.json$', views.connect_descriptor,
        name='bitbucket.descriptor'),
    url(r'^lifecycle/install$', views.install, name='bitbucket.install'),
    url(r'^lifecycle/uninstall$', views.uninstall, name='bitbucket.uninstall'),
]
