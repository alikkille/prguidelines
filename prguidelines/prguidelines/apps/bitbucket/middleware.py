from prguidelines.apps.bitbucket.models import BitbucketConnection


class JWTConnectionMiddleware(object):
    def process_request(self, request):
        """Sets the Bitbucket connection object if the request has a JWT claim.

        When the incoming request has a JWT token, this middleware gets the
        Bitbucket connection object and sets it for the current session and the
        request.

        When the incoming request has no JWT token, but the current session
        contains the corresponding Bitbucket connection object from an earlier
        request, this middleware sets the connection object on the request.
        """
        headers = getattr(request, 'META', {})
        if ('HTTP_AUTHORIZATION' in headers and
                'JWT' in headers['HTTP_AUTHORIZATION']):
            token = headers['HTTP_AUTHORIZATION'][4:]
            connection = BitbucketConnection.connection_from_jwt(token)
        elif request.method == 'GET' and 'jwt' in request.GET:
            token = request.GET.get('jwt')
            connection = BitbucketConnection.connection_from_jwt(token)
        else:
            # If the connection is in the session, then return it. If it's not
            # in the session, then all is hopeless and return None.
            connection = request.session.get('connection')

        # if the connection was not in the session, then add it in for future
        # use.
        request.session['connection'] = connection

        request.bb = connection
