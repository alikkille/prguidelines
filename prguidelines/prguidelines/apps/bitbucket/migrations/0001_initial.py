# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', models.CharField(unique=True, max_length=38, db_index=True)),
                ('username', models.CharField(unique=True, max_length=30)),
                ('display_name', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='BitbucketConnection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shared_secret', models.CharField(max_length=128)),
                ('client_key', models.CharField(unique=True, max_length=128, db_index=True)),
                ('account', models.OneToOneField(related_name='connection', to='bitbucket.Account')),
            ],
        ),
        migrations.CreateModel(
            name='Repository',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', models.CharField(unique=True, max_length=38, db_index=True)),
                ('name', models.CharField(max_length=62)),
                ('owner', models.ForeignKey(related_name='repos', to='bitbucket.Account')),
            ],
        ),
    ]
