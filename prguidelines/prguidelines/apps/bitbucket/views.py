import json

from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import IntegrityError
from django.http.response import HttpResponseBadRequest, HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt

from prguidelines.apps.bitbucket.models import BitbucketConnection


@require_GET
def connect_descriptor(request):
    descriptor = {
        "baseUrl": "https://{host}".format(host=request.get_host()),
        "name": "Pullrequest Guidelines",
        "description": ("An add-on that facilitates adherence to guidelines "
                        "for contributing to a repository."),
        "key": "prguidelines",
        "vendor": {
            "name": "Atlassian Inc.",
            "url": "http://www.atlassian.com"
        },
        "authentication": {
            "type": "jwt"
        },
        "contexts": ["account"],
        "lifecycle": {
            "installed": reverse('bitbucket.install'),
            "uninstalled": reverse('bitbucket.uninstall'),
        },
        "modules": {
            "webItems": [{
                "key": "pr-dialog-webitem",
                "name": {
                    "value": "Pull request guidelines",
                },
                "url": "/guidelines/repos/{repo_uuid}/{pullrequest_id}/dialog?"
                       "destination_commit={pullrequest_destination_commit}",
                "location": "org.bitbucket.pullrequest.summary.info",
                "context": "addon",
                "target": {
                    "type": "dialog",
                    "options": {
                        "width": "70%",
                        "height": "60%",
                        "chrome": False,
                    }
                },
                "params": {
                    "auiIcon": "aui-iconfont-review"
                }
            }],
        },
        "scopes": [
            "repository", "pullrequest:write", "snippet"
        ]
    }
    return HttpResponse(json.dumps(descriptor),
                        content_type='application/json')


@require_POST
@csrf_exempt
def install(request):
    event = json.loads(request.body.decode('utf-8'))

    if event['eventType'] != 'installed':
        return HttpResponseBadRequest(u'"eventType" was not "installed"')

    try:
        BitbucketConnection.install(event)
    except IntegrityError:
        return HttpResponseBadRequest('Addon could not be installed. Perhaps '
                                      'it is already installed on this repo.')
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def uninstall(request):
    event = json.loads(request.body.decode('utf-8'))

    if event['eventType'] != 'uninstalled':
        return HttpResponseBadRequest(u'"eventType" was not "uninstalled"')

    if not request.bb:
        return HttpResponseBadRequest()

    # The account's deletion cascade deletes the connection and everything else.
    request.bb.account.delete()

    return HttpResponse(status=200)
