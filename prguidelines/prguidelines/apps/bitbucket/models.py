import json
from hashlib import sha256
from time import time

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

import jwt
import requests
from django.db import models, transaction


def _hash_url(method, url):
    """Hash URL based on Connect hashing specification"""
    url = url or '/'
    qs = []
    if '?' in url:
        url, qs = url.split('?')
        qs = sorted(qs.split('&'))

    result = u'%s&%s&%s' % (method.upper(), url, '&'.join(qs))
    sha = sha256(result.encode('utf-8')).hexdigest()
    return sha


class Account(models.Model):
    uuid = models.CharField(max_length=38, unique=True, db_index=True)
    username = models.CharField(max_length=30, unique=True)
    display_name = models.CharField(max_length=60)


class Repository(models.Model):
    uuid = models.CharField(max_length=38, unique=True, db_index=True)
    name = models.CharField(max_length=62)
    owner = models.ForeignKey(Account, related_name='repos')


class BitbucketConnection(models.Model):
    shared_secret = models.CharField(max_length=128)
    client_key = models.CharField(max_length=128, db_index=True, unique=True)
    account = models.OneToOneField(Account, related_name='connection')
    host = models.CharField(max_length=128, default='https://bitbucket.org')

    def create_token(self, url, method='POST', timeout=3600000):
        payload = {'iss': 'prguidelines', 'iat': int(time()),
                   'exp': int(time()) + timeout, 'sub': self.client_key,
                   'qsh': _hash_url(method.upper(), url)}
        return jwt.encode(payload=payload, key=self.shared_secret,
                          algorithm='HS256').decode('ascii')

    @classmethod
    @transaction.atomic
    def install(cls, event):
        """Creates a Bitbucket connection using install lifecycle-event.

        If a connection with the provided client_key already exists, then
        installation will fail.
        """
        owner_data = event['principal']
        owner, owner_created = Account.objects.get_or_create(
            uuid=owner_data['uuid'], defaults={
                'username': owner_data['username'],
                'display_name': owner_data['display_name']})

        if not owner_created:
            # The account was already in our database. That's weird that
            # Bitbucket allowed a secondary installation, but it is possible
            # for completely isolated systems to go out of sync. We have to
            # handle this scenario gracefully.
            # Since this add-on doesn't store much state per repo, it is kinda
            # OK to just reinstall with the new key and secret, which means
            # that we have to clear out any existing connection.
            cls.objects.filter(account=owner).delete()

        # Attempt to create the connection.
        connection = cls.objects.create(
            client_key=event['clientKey'], account=owner,
            shared_secret=event['sharedSecret'], host=event['baseUrl']
        )

        return connection

    @classmethod
    def connection_from_jwt(cls, token):
        payload = jwt.decode(token, verify=False)
        connection = cls.objects.get(client_key=(payload['iss']))
        # In this version of PyJWT, I can't disable audience checking. This
        # means that I have to do a hacky fix where I use the payload iss to
        # do the audience check. It's OK because we have indirectly verified
        # the audience anyways, since BB uses shared_secret for the signing
        # algorithm in JWT, hence making sure that the addon was the only
        # possible audience (others don't have my shared secret).
        jwt.decode(token, connection.shared_secret, audience=payload['iss'])
        return connection

    def _authorization_header(self, url, method):
        token = self.create_token(url, method=method)
        return {'Authorization': 'JWT %s' % token}

    def get(self, url, params=None, decode_json=True):
        return self._send_request(method='get', url=url,
                                  decode_json=decode_json, params=params,
                                  allow_redirects=True)

    def post(self, url, data=None, decode_json=True):
        return self._send_request(method='post', url=url,
                                  decode_json=decode_json, data=data)

    def put(self, url, data=None, decode_json=True):
        return self._send_request(method='put', url=url,
                                  decode_json=decode_json, data=data)

    def _send_request(self, method, url, decode_json=True, **kwargs):
        url = urljoin(self.host, url)

        kwargs.setdefault('headers', {})
        kwargs['headers'].update(self._authorization_header(url, method))

        resp = requests.request(method, url, **kwargs)

        resp.raise_for_status()

        if decode_json:
            return json.loads(resp.text)
        else:
            return resp.text
