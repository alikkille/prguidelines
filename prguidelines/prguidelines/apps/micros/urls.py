from django.conf.urls import url
from prguidelines.apps.micros import views


urlpatterns = [
    url(r'^healthcheck$', views.healthcheck, name='micros.healthcheck'),
]
