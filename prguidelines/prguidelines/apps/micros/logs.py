import cgitb
import json
import logging
import datetime
import time
import traceback

import tzlocal
from django.conf import settings
from gunicorn import glogging

LOCAL_TZ = tzlocal.get_localzone()


class JSONFormatter(logging.Formatter):
    """A formatter that renders log records as JSON objects.
    Format: {"timestamp":"...", "level":"...", "name":"...", "message":"..."}

    This is the formatter used for all application level logging. The
    purpose is not to do access logging (that's done by the `GunicornLogger`
    below), but to log errors along with all the variables that could help
    diagnose the error (including a detailed traceback that includes the
    local variables at various frames of the stack).
    """
    def format(self, record):
        """Encode log record as JSON."""
        log = {
            "timestamp": self.formatTime(record, self.datefmt),
            "level": record.levelname,
            "name": record.name,
            "message": record.getMessage(),
        }
        if getattr(record, 'request', None) is not None:
            log['request'] = self.format_request(record)
        if record.exc_info:
            log["traceback"] = self.formatException(record.exc_info)

        indent = 4 if settings.DEBUG else None
        return json.dumps(log, indent=indent)

    @staticmethod
    def format_request(record):
        request = record.request
        d = {
            'method': request.method,
            'path': {
                'raw': request.path,
            },
            'host_name': request.get_host(),
            'qs': request.META.get('QUERY_STRING', '')
        }
        if getattr(request, 'resolver_match', None) is not None:
            d['path'].update({
                'name': request.resolver_match.url_name,
                'args': request.resolver_match.args,
                'kwargs': request.resolver_match.kwargs,
            })
        return d

    def formatException(self, ei):
        return cgitb.text(ei)

    def formatTime(self, record, datefmt=None):
        """Override default to use strftime, e.g. to get microseconds."""
        created = datetime.datetime.fromtimestamp(record.created, LOCAL_TZ)
        if datefmt:
            return created.strftime(datefmt)
        else:
            return created.strftime("%Y-%m-%dT%H:%M:%S%z".format(
                record.msecs))


class GunicornLogger(glogging.Logger):
    """Logger used by the gunicorn server.

    The purpose of this logger is for access logging. The reason for having
    this logger (instead of just using the Django logger) is because this
    operates at the Gunicorn level and hence is able to log even cached
    requests. Specifically, when the cache headers in a request indicate
    that the browser does indeed have the correct version of the page,
    this logger will record the request, while the Django level logger
    wouldn't see the request.
    """
    def now(self):
        return time.strftime('%Y-%m-%dT%H:%M:%S%z')

    def atoms(self, resp, req, environ, request_time):
        status = resp.status.split(None, 1)[0]
        maybe_nones = {
            'date': self.now(),
            'request_time_microseconds': ((request_time.seconds*1000000) +
                                          request_time.microseconds),
            'request_time_seconds': request_time.seconds,
            'remote_address': environ.get('REMOTE_ADDR'),
            'user_agent': environ.get('HTTP_USER_AGENT'),
            'referrer': environ.get('HTTP_REFERER'),
            'resp_len': resp.sent,
            'method': environ.get('REQUEST_METHOD'),
            'path': environ.get('RAW_URI'),
            'protocol': environ.get('SERVER_PROTOCOL'),
            'status': status,
            'name': 'gunicorn',
            'level': 'INFO',
        }

        return {k: v for k, v in maybe_nones.iteritems() if v is not None}

    def access(self, resp, req, environ, request_time):
        """Override the superclass' access to circumvent the restrictions of
        access_log_format.

        Gunicorn's access_log_format forces you to include atoms even when
        they are `None` by replacing them with `-`. That is not acceptable.
        """
        if not self.cfg.accesslog and not self.cfg.logconfig:
            return

        atoms = self.atoms(resp, req, environ, request_time)

        try:
            indent = 4 if settings.DEBUG else None
            self.access_log.info(json.dumps(atoms, indent=indent))
        except:
            self.error(traceback.format_exc())
