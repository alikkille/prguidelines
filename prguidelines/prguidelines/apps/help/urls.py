from django.conf import settings
from django.conf.urls import url
from django.views.generic.base import TemplateView

from prguidelines.apps.help import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='homepage.html'),
        name='help.homepage', kwargs={'BITBUCKET_URL': settings.BITBUCKET_URL,
                                      'CANON_URL': settings.CANON_URL}),
    url(r'^installation_redirect', views.InstallationRedirect.as_view(),
        name='help.installation_redirect',
        kwargs={'BITBUCKET_URL': settings.BITBUCKET_URL,
                'CANON_URL': settings.CANON_URL}),
]
