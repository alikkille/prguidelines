from django.views.generic import TemplateView


class InstallationRedirect(TemplateView):
    template_name = 'installation_redirect.html'

    def get_context_data(self, **kwargs):
        ctx = super(InstallationRedirect, self).get_context_data(**kwargs)
        ctx.update(error=self.request.GET.get('error'),
                   error_description=self.request.GET.get('error_description'))
        return ctx
