from django.conf.urls import url
from prguidelines.apps.guidelines import views


urlpatterns = [
    url(r'^repos/(?P<repo_uuid>[^/]+)/(?P<pr_id>\d+)/dialog$',
        views.guidelines_dialog),
]
