import json
import pytest
import requests
import time

from httptest import testserver
from textwrap import dedent

from prguidelines.apps.guidelines.exceptions import (GImportError,
                                                     GImportTimeout)
from prguidelines.apps.guidelines.parser import parse_guidelines


class BBMock(object):
    def get(self, url, params=None, decode_json=True):
        resp = requests.get(url, params)
        resp.raise_for_status()
        return resp.text if not decode_json else json.loads(resp.text)


def test_that_top_description_is_ignored():
    guidelines = parse_guidelines(dedent(u"""\
    Top level description that will be ignored.

    # Topic title

    Topic Description
    """), None)
    assert len(guidelines) == 1
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u'Topic Description'
    assert len(guidelines[0].sections) == 0


def test_that_header_and_content_can_be_close_together():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title
    Topic Description
    """), None)
    assert len(guidelines) == 1
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u'Topic Description'
    assert len(guidelines[0].sections) == 0


def test_that_header_and_content_can_have_newlines_between_them():
    guidelines = parse_guidelines(dedent(u"""\
    # First topic title

    Topic Description after one newline.

    # Second topic title
    Topic description after no newlines.

    # Third topic title


    Topic description with two newlines after topic.
    """), None)
    assert len(guidelines) == 3
    assert guidelines[0].title == u'First topic title'
    assert guidelines[0].description == u'Topic Description after one newline.'
    assert guidelines[1].title == u'Second topic title'
    assert guidelines[1].description == u'Topic description after no newlines.'
    assert guidelines[2].title == u'Third topic title'
    assert guidelines[2].description == (u'Topic description with two '
                                         u'newlines after topic.')


def test_that_topic_descriptions_can_be_multi_paragraph():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title

    First paragraph.

    Second paragraph.

    # Another topic title
    First paragraph without space above.
    Still part of the first paragraph.


    Second paragraph with two spaces above.
    """), None)
    assert len(guidelines) == 2
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == dedent(u"""\
    First paragraph.

    Second paragraph.""")
    assert guidelines[1].title == u'Another topic title'
    assert guidelines[1].description == dedent(u"""\
    First paragraph without space above.
    Still part of the first paragraph.

    Second paragraph with two spaces above.""")


def test_that_topic_descriptions_can_contain_non_paragraphs():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title

    ![Image](http://image.com/foobar.png)

    Some description can be added too.

    ```python

    import sys
    ```
    """), None)
    assert len(guidelines) == 1
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == dedent(u"""\
    ![Image](http://image.com/foobar.png)

    Some description can be added too.

    ```python

    import sys
    ```""")


def test_that_topics_can_have_sections():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title

    Topic description.

    ## First section
    Section content

    ## Second section

    Section content

    More section content.
    """), None)
    assert len(guidelines) == 1
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u'Topic description.'

    sections = guidelines[0].sections
    assert len(sections) == 2
    assert sections[0].title == u'First section'
    assert sections[0].content == u'Section content'
    assert sections[1].title == u'Second section'
    assert sections[1].content == dedent(u"""\
    Section content

    More section content.""")


def test_that_topics_and_guidelines_dont_have_to_have_descriptions():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title

    ## First section
    ## Second section
    # Second topic
    """), None)
    assert len(guidelines) == 2
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u''

    sections = guidelines[0].sections
    assert len(sections) == 2
    assert sections[0].title == u'First section'
    assert sections[0].content == u''
    assert sections[1].title == u'Second section'
    assert sections[1].content == u''

    assert guidelines[1].title == u'Second topic'
    assert guidelines[1].description == u''


def test_that_topics_can_be_both_hash_headers_and_setext_headers():
    guidelines = parse_guidelines(dedent(u"""\
    # Topic title #

    First section
    -------------
    ## Second section ##
    Second topic
    ============
    """), None)
    assert len(guidelines) == 2
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u''

    sections = guidelines[0].sections
    assert len(sections) == 2
    assert sections[0].title == u'First section'
    assert sections[0].content == u''
    assert sections[1].title == u'Second section'
    assert sections[1].content == u''

    assert guidelines[1].title == u'Second topic'
    assert guidelines[1].description == u''


def test_that_h2_before_h1_doesnt_cause_errors():
    guidelines = parse_guidelines(dedent(u"""\
    Overall description.

    ## A section before a topic
    Section description

    # Topic title

    ## Topic section
    """), None)
    assert len(guidelines) == 1
    assert guidelines[0].title == u'Topic title'
    assert guidelines[0].description == u''
    assert len(guidelines[0].sections) == 1
    assert guidelines[0].sections[0].title == u'Topic section'
    assert guidelines[0].sections[0].content == u''


def test_that_headers_in_code_blocks_render_verbatim():
    guidelines = parse_guidelines(dedent(u"""\
    # Level 1 header

    ```
    # Level 1 header in code block

    Some description for level 1 header.

    ## Level 2 header in code block

    Some description for level 2 header.
    ```

    ## Real level 2 header

    Level 2 header description.

    ```
    # Why is there a level 1 here?
    ```

    # Another level 1 header
    """), None)
    assert len(guidelines) == 2
    assert guidelines[0].title == u'Level 1 header'
    assert guidelines[0].description.endswith(u'Some description for '
                                              u'level 2 header.\n```')
    assert len(guidelines[0].sections) == 1
    assert guidelines[0].sections[0].title == u'Real level 2 header'
    assert guidelines[1].title == u'Another level 1 header'


def test_that_top_level_imports_can_pull_in_an_accessible_file(settings):
    def app(environ, start_response):
        content_by_path = {
            'path/to/raw/file.md': json.dumps({'data': dedent("""\
            # First external topic
            ## First external section
            """)}),
            'yet/another/raw/file.md': json.dumps({'data': dedent("""\
            # Second external topic
            ## Second external section
            """)})
        }
        path = environ.get('PATH_INFO', '').lstrip('/')
        start_response('200 OK',
                       [('Content-type', 'application/json; charset=utf-8')])
        return [content_by_path[path].encode('utf-8')]

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        guidelines = parse_guidelines(dedent(u"""\
        <!-- import path/to/raw/file.md -->
        Other top level description.
        <!-- import yet/another/raw/file.md -->

        # Topic title
        """), BBMock())
        assert len(guidelines) == 3
        assert guidelines[0].title == u'First external topic'
        assert len(guidelines[0].sections) == 1
        assert guidelines[0].sections[0].title == u'First external section'
        assert guidelines[1].title == u'Second external topic'
        assert len(guidelines[1].sections) == 1
        assert guidelines[1].sections[0].title == u'Second external section'


def test_that_import_comments_in_descriptions_are_ignored(settings):
    def app(environ, start_response):
        pytest.fail('Import from a topic description was attempted.')

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        guidelines = parse_guidelines(dedent(u"""\
        <!-- random comment -->
        # Topic title
        <!-- import foobar -->
        """), BBMock())
        assert len(guidelines) == 1
        assert guidelines[0].title == u'Topic title'
        assert guidelines[0].description == u'<!-- import foobar -->'


def test_that_illegal_import_is_reported_back(settings):
    def app(environ, start_response):
        path = environ.get('PATH_INFO', '').lstrip('/')
        if path == 'legal_path':
            start_response('200 OK',
                           [('Content-type', 'text/plain; charset=utf-8')])
            return [json.dumps({
                'data': '# Legal external topic'
            }).encode('utf-8')]
        else:
            start_response('404 NOT FOUND',
                           [('Content-type', 'text/html; charset=utf-8')])
            return [b'']

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        guidelines = parse_guidelines(dedent(u"""\
        <!-- import illegal_path -->
        <!-- import legal_path -->
        """), BBMock())
        assert len(guidelines) == 1
        assert guidelines[0].title == u'Legal external topic'
        assert len(guidelines.errors) == 1
        assert isinstance(guidelines.errors[0], GImportError)
        assert guidelines.errors[0].url.endswith(u'/illegal_path')


def test_that_import_timeout_is_respected(settings):
    def app(environ, start_response):
        path = environ.get('PATH_INFO', '').lstrip('/')
        sleep_time = float(path)
        time.sleep(sleep_time)
        start_response('200 OK',
                       [('Content-type', 'text/plain; charset=utf-8')])
        return [json.dumps(
            {'data': '# Slept for %0.2f seconds' % sleep_time}
        ).encode('utf-8')]

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        settings.IMPORT_QUOTA = 0.1
        guidelines = parse_guidelines(dedent(u"""\
        <!-- import 0.05 -->
        <!-- import 0.06 -->
        <!-- import 0.02 -->
        """), BBMock())
        assert len(guidelines) == 1
        assert guidelines[0].title == u'Slept for 0.05 seconds'
        assert len(guidelines.errors) == 2
        assert isinstance(guidelines.errors[0], GImportTimeout)
        assert guidelines.errors[0].url.endswith(u'0.06')
        assert isinstance(guidelines.errors[1], GImportTimeout)
        assert guidelines.errors[1].url.endswith(u'0.02')


def test_that_external_import_can_return_interesting_charactors(settings):
    def app(environ, start_response):
        resp_bytes = json.dumps({'data': u'# \U0001F4A9'}).encode('utf-8')
        start_response('200 OK',
                       [('Content-type', 'text/plain; charset=utf-8')])
        return [resp_bytes]

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        guidelines = parse_guidelines(dedent(u"""\
        <!-- import does/not/matter -->
        """), BBMock())
        assert len(guidelines) == 1
        assert guidelines[0].title == u'\U0001F4A9'


def test_that_cyclical_imports_dont_loop_forever(settings):
    calls = [0]

    def app(environ, start_response):
        calls[0] += 1
        path = environ.get('PATH_INFO', '').lstrip('/')
        if path == 'ping':
            resp = json.dumps({'data': '<!-- import pong -->'}).encode('utf-8')
        else:
            resp = json.dumps({'data': '<!-- import ping -->'}).encode('utf-8')
        start_response('200 OK',
                       [('Content-type', 'text/plain; charset=utf-8')])
        return [resp]

    with testserver(app) as server:
        settings.BITBUCKET_URL = server.url().rstrip('/')
        parse_guidelines(dedent(u"""\
        <!-- import ping -->
        """), BBMock())
        assert calls[0] == 2
