import re
from urlparse import urlparse, urlunparse

import markdown

from django.conf import settings
from interruptingcow import timeout, Quota
from markdown.blockparser import BlockParser
from markdown.blockprocessors import (HashHeaderProcessor,
                                      SetextHeaderProcessor,
                                      EmptyBlockProcessor,
                                      ParagraphProcessor)
from requests import HTTPError

from prguidelines.apps.guidelines.exceptions import (GImportTimeout,
                                                     GImportError)
from prguidelines.apps.md import render


IMPORT_RE = re.compile(r'^<!-- import (?P<url>\S+) -->$', re.MULTILINE)

md = markdown.Markdown(safe_mode=u'escape',
                       extensions=[],
                       extension_configs={},
                       enable_attributes=False)
md.parser = BlockParser(md)
md.parser.blockprocessors['empty'] = EmptyBlockProcessor(md.parser)
md.parser.blockprocessors['hashheader'] = HashHeaderProcessor(md.parser)
md.parser.blockprocessors['setextheader'] = SetextHeaderProcessor(md.parser)
md.parser.blockprocessors['paragraph'] = ParagraphProcessor(md.parser)


class Guidelines(object):
    def __init__(self, topics=None, errors=None):
        self.topics = topics or []
        self.errors = errors or []

    def __nonzero__(self):
        return bool(self.topics)

    def __len__(self):
        return len(self.topics)

    def __getitem__(self, item):
        return self.topics[item]

    def add_topics(self, *topics):
        self.topics.extend(topics)

    def add_sections(self, *sections):
        """Adds a section to the newest topic."""
        assert self.topics
        self.topics[-1].sections.extend(sections)

    def add_errors(self, *errors):
        self.errors.extend(errors)

    def extend(self, guidelines):
        self.add_errors(*guidelines.errors)
        self.add_topics(*guidelines.topics)

    @property
    def last_topic(self):
        assert self.topics
        return self.topics[-1]


class Topic(object):
    def __init__(self, title, sections=None, description=''):
        self.title = title
        self.sections = sections or []
        self.description = description

    def __len__(self):
        return len(self.sections)

    @property
    def rendered(self):
        return render(self.description)

    @property
    def last_section(self):
        assert self.sections
        return self.sections[-1]


class Section(object):
    def __init__(self, title, content=''):
        self.title = title
        self.content = content
        self._rendered = None

    @property
    def rendered(self):
        if self._rendered is None:
            self._rendered = render(self.content)
        return self._rendered


def parse_guidelines(contents, bb, import_quota=None, visited_urls=None):
    """Processes contents from a single guidelines file.

    A guidelines file consists of:

      <optional description> (con contain imports)

      # Topic

      <optional topic description>

      ## Section

      <optional section description>

      # Topic

      <optional topic description>

      ## Section

      <optional section description>

      ... (etc.)

    This method processes such a string and returns a list of ``Topic``
    objects.

    :param str contents: The contents of a single guidelines file.
    :param prguidelines.apps.bitbucket.models.BitbucketConnection bb:
    :param int import_quota: Number of seconds that will be spent downloading
      the imports.
    :param set visited_urls: The set of URLs that have already been imported
      and should not be imported again.
    """
    import_quota = import_quota or Quota(settings.IMPORT_QUOTA)
    if visited_urls is None:
        visited_urls = set()

    try:
        source = markdown.util.text_type(contents)
    except UnicodeDecodeError as e:
        # Customise error message while maintaining original trackback
        e.reason += '. -- Note: Markdown only accepts unicode input!'
        raise

    # Split into lines and run the line preprocessors.
    lines = source.split("\n")
    for prep in md.preprocessors.values():
        lines = prep.run(lines)

    # Parse the high-level elements.
    root = md.parser.parseDocument(lines).getroot()

    guidelines = Guidelines()
    fenced = False  # Whether we are inside a fence

    def add_text_to_tail(text):
        if not guidelines:
            return
        elif guidelines.last_topic.sections:
            last_section = guidelines.last_topic.last_section
            if last_section.content:
                last_section.content += '\n\n' + text
            else:
                last_section.content = text
        else:
            last_topic = guidelines.last_topic
            if last_topic.description:
                last_topic.description += '\n\n' + text
            else:
                last_topic.description = text

    for node in root:
        # determine if we are fenced
        if node.text and (node.text.startswith('```')
                          or node.text.endswith('\n```')):
            fenced = not fenced
            # If we just got out of a fence block, this node needs to be added
            # verbatim too
            if not fenced:
                add_text_to_tail(node.text)
                continue

        if fenced:
            # If fenced, it doesn't matter whether we're at a header. The text
            # stays verbatim.
            add_text_to_tail(node.text)
        elif node.tag == u'h1':
            guidelines.add_topics(Topic(title=node.text))
        elif node.tag == u'h2' and guidelines:
            guidelines.add_sections(Section(title=node.text))
        else:
            if not guidelines:
                import_urls = IMPORT_RE.findall(node.text)
                for i, url in enumerate(import_urls):
                    if url in visited_urls:
                        continue
                    visited_urls.add(url)
                    try:
                        with timeout(import_quota, GImportTimeout):
                            imported_contents = _import_from_url(url, bb)
                    except GImportTimeout:
                        # Out of quota. Stop importing. Mark this URL and all
                        # others following it as timed out.
                        guidelines.add_errors(*(GImportTimeout(url=url)
                                                for url in import_urls[i:]))
                        break
                    except GImportError as e:
                        guidelines.add_errors(e)
                        continue
                    guidelines.extend(parse_guidelines(imported_contents, bb,
                                                       import_quota,
                                                       visited_urls))
            else:
                add_text_to_tail(node.text)

    return guidelines


def _import_from_url(url, bb):
    parsed = urlparse(url)
    parsed_bb = urlparse(settings.BITBUCKET_URL)
    normalized_url = urlunparse((parsed_bb.scheme, parsed_bb.netloc,
                                 parsed.path, parsed.params, parsed.query,
                                 parsed.fragment))
    try:
        resp = bb.get(normalized_url)
    except HTTPError as e:
        raise GImportError(e.message, url=normalized_url)
    try:
        return resp['data']
    except KeyError as e:
        raise GImportError(e.message, url=normalized_url)
