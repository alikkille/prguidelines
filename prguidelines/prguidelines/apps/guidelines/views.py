from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import RequestContext
from requests import HTTPError

from prguidelines.apps.guidelines.parser import parse_guidelines


def guidelines_dialog(request, repo_uuid, pr_id):
    if request.bb is None:
        return HttpResponseBadRequest('Valid session could not be found')

    account = request.bb.account
    revision = request.GET['destination_commit']

    # Check if the current repo has a CONTRIBUTING.md file
    raw_file_url = ('api/1.0/repositories/{acc.uuid}/{repo_uuid}/src/'
                    '{revision}/CONTRIBUTING.md')
    try:
        contr_f_resp = request.bb.get(raw_file_url.format(
            acc=account, repo_uuid=repo_uuid,
            revision=revision
        ))
    except HTTPError as e:
        if e.response.status_code == 404:  # Not found
            # This just means that the repo doesn't have a CONTRIBUTING.md
            # file and hence must be shown the blank-slate.
            return render_to_response('guidelines/blank-slate-dialog.html',
                                      RequestContext(request))
        else:
            raise

    contr_f = contr_f_resp['data']

    guidelines = parse_guidelines(contr_f, request.bb)

    return render_to_response('guidelines/dialog.html', RequestContext(
        request, {
            'guidelines': guidelines,
            'source_url': '{host}/{acc.uuid}/{repo_uuid}/src/{revision}/'
                          'CONTRIBUTING.md'.format(host=request.bb.host,
                                                   acc=account,
                                                   repo_uuid=repo_uuid,
                                                   revision=revision)
        }
    ))
