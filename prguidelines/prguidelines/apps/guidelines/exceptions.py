class GImportError(Exception):
    def __init__(self, *args, **kwargs):
        self.url = kwargs.pop('url', None)
        super(GImportError, self).__init__(*args, **kwargs)

    @property
    def message(self):
        return "Could not import URL ({url}): {reason}".format(
            url=self.url, reason=super(GImportError, self).message
        )


class GImportTimeout(GImportError):
    @property
    def message(self):
        if self.url is not None:
            return 'Import of {url} timed out.'.format(url=self.url)
        else:
            return 'Import timed out'
