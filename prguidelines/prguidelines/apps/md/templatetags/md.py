from textwrap import dedent

from django import template
from django.utils.safestring import mark_safe

from prguidelines.apps.md import render


register = template.Library()


@register.tag(name="markdown")
def markdown_tag(parser, token):
    nodelist = parser.parse(('endmarkdown',))
    parser.delete_first_token()  # consume '{% endmarkdown %}'
    return MarkdownNode(nodelist)


class MarkdownNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        value = self.nodelist.render(context)
        return mark_safe(render(dedent(value)))
