import markdown
from markdown.extensions.headerid import slugify

extensions = ['codehilite', 'tables', 'def_list', 'footnotes',
              'headerid', 'sane_lists', 'abbr', 'fenced_code', 'toc']

extension_configs = {
    'headerid': [('slugify',
                  lambda value, separator:
                  'markdown-header-' + slugify(value, separator))],
    'codehilite': [('linenums', False)]
}

md = markdown.Markdown(safe_mode=u'escape',
                       extensions=extensions,
                       extension_configs=extension_configs,
                       enable_attributes=False)

render = md.convert
